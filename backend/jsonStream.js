const { Database, aql } = require('arangojs');
const fs = require('fs');
const JSONStream = require('JSONStream');
const es = require('event-stream');

const input = './data.json';

const db = new Database({
    url: "http://localhost:8529",
    auth: { username: "root", password: "123" },
    databaseName: 'Aflam',

});

/* (async () => {
    try {
        //const persons = await 
        db.query(aql`UPSERT {imdbId:'nm4862056'} 
    INSERT {imdbId: "nm4862056",name: "Niamh Algar"} 
    UPDATE {} IN Person 
    RETURN NEW`).then(p => console.log(p.next()));
        // for await (const person of persons) {
        //     console.log(person._id);
        // }
    } catch (err) {
        console.log(err);
    }
})(); */

const getStream = () => {
    const jsonData = input;
    const stream = fs.createReadStream(jsonData, { encoding: 'utf8' });
    const parser = JSONStream.parse('*');

    return stream.pipe(parser);
};
//, ...Object.keys(data.credits).filter(i => i.indexOf('star') === -1).map(dx => data.credits[dx]).flat()
async function main() {
    try {
        getStream().pipe(es.mapSync(async data => {
            //await db.query(aql`
            const q = (aql`
                LET production = ${extractProduction(data)}
                LET keywords = ${data.keywords.map(v => ({ name: v }))}
                LET genres = ${data.genres.map(v => ({ name: v }))}
                LET companies = ${data.producers}
                LET persons = ${data.cast}

                LET pId = (
                    UPSERT {imdbId:production.imdbId} INSERT production UPDATE{} INTO Production 
                    RETURN NEW._id
                )[0]
                
                LET gIds = FLATTEN(
                    FOR g IN genres
                        UPSERT {name:g.name} INSERT g UPDATE{} INTO Genre
                        RETURN {id:NEW._id}
                )
                LET kIds = FLATTEN(
                    FOR kw IN keywords
                        UPSERT {name:kw.name} INSERT kw UPDATE{} INTO Keyword
                        RETURN {id:NEW._id}
                )
                LET comIds = (
                    FOR com IN companies
                        UPSERT {imdbId:com.imdbId} INSERT com UPDATE{} INTO Company
                        RETURN {id:NEW._id}
                )
                LET actorIds = (
                    FOR per IN persons
                        UPSERT {imdbId:per.imdbId} INSERT per UPDATE{} INTO Person
                        RETURN {id:NEW._id}
                )
                LET temp = (
                    LET a = (FOR v IN gIds UPSERT {_from:v.id,_to:pId} INSERT {_from:v.id,_to:pId} UPDATE{} INTO genre_of)
                    LET b = (FOR v IN kIds UPSERT {_from:v.id,_to:pId} INSERT {_from:v.id,_to:pId} UPDATE{} INTO keyword_of)
                    LET c = (FOR v IN comIds UPSERT {_from:v.id,_to:pId} INSERT {_from:v.id,_to:pId} UPDATE{} INTO producer_of)
                    LET d = (FOR v IN actorIds UPSERT {_from:v.id,_to:pId} INSERT {_from:v.id,_to:pId} UPDATE{} INTO actor_in)
                    return 'Done!'
                )
                FOR v IN temp return v
            `);
            //fs.writeFileSync('aql.txt', q);
            db.query(q);
        }));
    } catch (err) {
        console.log(err);
    }
};

main().then(() => {
    console.log('done');
    db.close();
});

function extractProduction(data) {
    return {
        imdbId: data.imdbId,
        name: data.name,
        poster: data.poster,
        year: data.year,
        ...(data.metascore ? { metascore: data.metascore } : null),
        ...(data.rating ? { rating: data.rating } : null),
        ...(data.raters ? { raters: data.raters } : null),
        ...(data.runtime ? { runtime: data.runtime } : null),
        ...(data.language?.length > 0 ? { language: data.language } : null),
        ...(data.country?.length > 0 ? { country: data.country } : null),
        ...(data.locations?.length > 0 ? { locations: data.locations } : null),
        ...(data.releaseDates ? { releaseDates: data.releaseDates } : null),
        ...(data.summary ? { summary: data.summary } : null)
    }
};
/*
const production = extractProduction(data);
            const p = await db.query(aql`UPSERT { imdbId: ${production.imdbId} } INSERT ${production} UPDATE {} IN Production RETURN NEW`);
            const insProduction = p.hasNext ? p.next() : null;
            data.keywords.forEach(kw => {
                db.query(aql`UPSERT { name: ${kw} } INSERT { name: ${kw} } UPDATE {} IN Keyword RETURN OLD`).then(async c => {
                    // link keyword to production
                    if (c.hasNext && insProduction) {

                        const insKw = c.next();
                        await db.query(aql`UPSERT { _from: ${insKw._id}, _to: ${insProduction._id} }
                        INSERT { _from: ${insKw._id}, _to: ${insProduction._id} }
                        UPDATE {}
                        IN keyword_of`);
                    }
                });
            });
            data.genres.forEach(v => {
                db.query(aql`UPSERT { name: ${v} } INSERT { name: ${v} } UPDATE {} IN Genre RETURN OLD`).then(async c => {
                    if (c.hasNext && insProduction) {
                        const rec = c.next();
                        await db.query(aql`UPSERT { _from: ${rec._id}, _to: ${insProduction._id} }
                        INSERT { _from: ${rec._id}, _to: ${insProduction._id} }
                        UPDATE {}
                        IN genre_of`);
                    }
                });
            })
            //data.genres.forEach(async v => await db.query(aql`UPSERT { name: ${v} } INSERT { name: ${v} } UPDATE {} IN Genre`));
            //data.cast.forEach(async v => await db.query(aql`UPSERT { imdbId: ${v.imdbId}} INSERT {imdbId:${v.imdbId},name:${v.name}} UPDATE{} IN Person`));
            */





