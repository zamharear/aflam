const puppeteer = require('puppeteer');
const fs = require('fs');

const protocol = 'https';
const server = `yifytorrent.unblockit.top`;
const MoviesList = JSON.parse(fs.readFileSync('yify_movies.json'));
const startTime = Date.now();
const last = Object.values(MoviesList).pop();

(async () => {

    const browser = await puppeteer.launch({
        headless: true,
        userDataDir: './cache',
        args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage', '--disable-gpu']
    });
    const page = await browser.newPage();

    await page.setJavaScriptEnabled(false);
    await page.goto(`${protocol}://${server}/popular-${last}.html`);
    for (let i = last; i < 505; i++) {
        // 1- scrap content
        console.log(`scrapping contents of Page #${i + 1} - ${(Date.now() - startTime) / 1000 / 60}m`);
        const names = await page.$$eval('.movie_list>article>figure h3>a', all => all.map((elm) => {
            const str = elm.innerText.toLowerCase().replace(/[ ]/g, '_');
            const name = str.substring(0, str.lastIndexOf(')') + 1);
            const y = name.match(/\(([0-9]{4})\)/);
            if (parseInt(y[1]) > 2015) {
                return name;
            }
        }));
        names.filter(e => (e)).forEach(n => MoviesList[`${n}`] = i + 1);
        fs.writeFileSync('yify_movies.json', JSON.stringify(MoviesList));

        try {
            await page.waitForTimeout((15 + Math.floor(Math.random() * 15)) * 1000);
            await page.click('#content .pagination .active+li>a');

        } catch (err) {
            console.log('no more pages! done.');
            await browser.close();
            break;
        }
    }
    await browser.close();
    //console.log(MoviesList);


    //await page.screenshot({ path: 'example.png' });



})();

