const fs = require('fs');
const http = require('http');
const fetch = require('node-fetch');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const imdbsuggestion = 'https://v2.sg.media-imdb.com/suggestion/titles'
const imdb_baseurl = 'https://www.imdb.com';

const requestListener = async function (req, res) {
    //fetch
    const response = await fetch(imdbsuggestion + req.url);
    const json = await response.json();
    const targetId = json.d.find(e => (e.q === 'feature' || e.q === 'video')).id;
    fetch(`${imdb_baseurl}/title/${targetId}/`)
        .then(async r => {
            if (r.ok) {
                const html = await r.text();
                const body = JSON.stringify({ ...scrapeMovie(html), imdbId: targetId });
                //fs.writeFile('./backend/output/last.html', html, err => err ? console.log(err) : null);
                //fs.appendFile('./backend/output/db.json', body + ',', err => err ? console.log(err) : null);
                res.writeHead(200, {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'X-Requested-With'
                }).end(body);
                //r.text().then(html => fs.writeFileSync('test_movies.json', JSON.stringify(scrapeMovie(html))));
            } else {
                console.error('Error', r.statusText);
            }
        });

};

function scrapeMovie(html) {
    const { document } = (new JSDOM(html)).window;
    return {
        poster: document.querySelector('.poster img').src,
        name: document.querySelector('.title_wrapper>h1').textContent.replace(/[()0-9]/g, '').trim(),
        year: document.querySelector('.title_wrapper>h1>#titleYear>a').textContent.trim(),
        raters: document.querySelector('.imdbRating .ratingValue>strong').title.replace(/[ ,]/g, '').match(/on([0-9]+)user/)[1],
        rating: document.querySelector('.imdbRating .ratingValue span[itemprop="ratingValue"]').textContent,
        metascore: document.querySelector('.metacriticScore>span')?.textContent,
        runtime: document.querySelector('.title_wrapper>.subtext>time')?.textContent.trim(),
        genres: [...document.querySelectorAll('#titleStoryLine .see-more>a[href*="genres"]')].map((e) => e.textContent.trim()),
        keywords: [...document.querySelectorAll('#titleStoryLine .see-more>a[href*="keywords"]')].map((e) => e.textContent.trim()),
        language: [...document.querySelectorAll('#titleDetails .txt-block> a[href*="language"]')].map((e) => e.textContent),
        producers: [...document.querySelectorAll('#titleDetails .txt-block> a[href*="company/co"]')].map((e) => ({ imdbId: e.href.match(/co[0-9]+/)[0], name: e.textContent.trim() })),
        country: [...document.querySelectorAll('#titleDetails>.txt-block a[href*="country_of_origin"]')].map((e) => e.textContent),
        releaseDates: document.querySelector('.title_wrapper>.subtext>a[title*="release dates"]')?.textContent.replace(/[ ]{2,}/g, ' ').trim(),
        cast: [...document.querySelectorAll('#titleCast .cast_list td:nth-of-type(2)>a[href*="name"]')].map((e) => ({ imdbId: e.href.match(/nm[0-9]+/)[0], name: e.textContent.trim() })),
        locations: document.querySelector('#titleDetails>.txt-block>a[href*="locations"]')?.textContent.replace(/,\s/g, ',').split(','),
        //summary: document.querySelector('.plot_summary .summary_text')?.textContent.trim(),
        summary: document.querySelector('#titleStoryLine>.canwrap>p')?.textContent.trim(),
        credits: [...document.querySelectorAll('.plot_summary .credit_summary_item')].reduce((credit, elm) => ({
            ...credit, [`${elm.querySelector('.inline').textContent.toLowerCase().replace(':', '')}`]:
                [...elm.querySelectorAll('a[href^="/name"]')].map((e) => ({ imdbId: e.href.match(/nm[0-9]+/)[0], name: e.textContent.trim() }))
        }), {}),
    }
};


const server = http.createServer(requestListener);
server.listen(8000);