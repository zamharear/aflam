const fs = require('fs');
const fetch = require('node-fetch');
const jsdom = require('jsdom');
const { mainModule } = require('process');
const { JSDOM } = jsdom;
const imdbsuggestion = 'https://v2.sg.media-imdb.com/suggestion/titles'
const imdb_baseurl = 'https://www.imdb.com';

// pathes:{title/${movieId}/(criticreviews|reviews), name/${personId},}

// 1- load yifiList from file;
const moviesList = Object.keys(JSON.parse(fs.readFileSync('./backend/input/yify_movies.json')));
const last = fs.readFileSync('./backend/input/cur_name', { encoding: 'utf8' });
while (moviesList[0] !== last) moviesList.shift();
const startTime = Date.now();
let toId = 0;
let count = 0;
let sav = 0;
let rej = 0;


const loop = () => {
    try {
        count++;
        main(moviesList.shift())
            .then(() => {
                if (moviesList.length > 0) {
                    toId = setTimeout(loop, (3 + Math.floor(Math.random() * 5)) * 1000);
                } else {
                    console.log('DONE DONE DONE');
                    return;
                }
            });
    } catch (err) {
        clearTimeout(toId);
        console.log('FAITAL ERROR', err);
        fs.writeFileSync('./backend/input/yifi_movies', JSON.stringify(moviesList));
    }
};
loop();
//----
async function main(mn) {
    fs.writeFile('./backend/input/cur_name', mn, err => err ? console.log(err) : null);
    nm = mn.replace(/^\W+/, '').replace(/[\?!#\-@\.\/&%]/g, '');
    let mId = await getId(nm);
    if (!mId) {
        mId = await getId(mn.replace(/_\([0-9]+\)/, '').replace(/[\?!#\-@\.\/&%]/g, ''));
    }
    if (!mId) {
        console.log(`${nm} not found!!!`);
        fs.appendFile('./backend/output/notfound', JSON.stringify(nm) + ',', err => err ? console.log(err) : null);
        return;
    }
    const mJson = await getMovie(mId);
    if (!mJson) {
        console.log(`${nm} not found!!!`);
        fs.appendFile('./backend/output/notfound', JSON.stringify(nm) + ',', err => err ? console.log(err) : null);
        return;
    }
    if (parseFloat(mJson.rating) > 5.5) {
        console.log(`#${elapsed()}m ${mJson.name} [${mJson.rating}] ------>${++sav} SAVED (${count}/${moviesList.length})`);
        fs.appendFile('./backend/output/saved.json', JSON.stringify(mJson) + ',', err => err ? console.log(err) : null);
    } else {
        console.log(`#${elapsed()}m ${mJson.name} [${mJson.rating}] ------>${++rej} REJECTED (${count}/${moviesList.length})`);
        fs.appendFile('./backend/output/rejected.json', JSON.stringify({ [mn]: mJson.rating }) + ',', err => err ? console.log(err) : null);
    }
};


async function getId(movieName) {
    //console.log(movieName + '\n');
    const response = await fetch(imdbsuggestion + `/${movieName.charAt(0)}/${movieName}.json`);
    const json = await response.json();
    if (typeof json.d === 'undefined') {
        return null;
    }
    return await json.d.find(e => (e.q === 'feature' || e.q === 'video'))?.id;
};

async function getMovie(imdbId) {
    const res = await fetch(`${imdb_baseurl}/title/${imdbId}/`);
    if (res.ok) {
        const html = await res.text();
        fs.writeFile('./backend/output/last.html', html, err => err ? console.log(err) : null);
        return ({ ...scrapeMovie(html), imdbId });
    } else {
        console.error(`Response Error:`, res);
        //break;
    }
};

function scrapeMovie(html) {
    const { document } = (new JSDOM(html)).window;
    const poster = document.querySelector('.poster img')?.src;
    const name = document.querySelector('.title_wrapper>h1')?.textContent.replace(/[()0-9]/g, '').trim();
    if (!poster || !name) {
        return null;
    }
    return {
        poster,
        name,
        year: document.querySelector('.title_wrapper>h1>#titleYear>a')?.textContent.trim(),
        raters: document.querySelector('.imdbRating .ratingValue>strong')?.title.replace(/[ ,]/g, '').match(/on([0-9]+)user/)[1],
        rating: document.querySelector('.imdbRating .ratingValue span[itemprop="ratingValue"]')?.textContent,
        metascore: document.querySelector('.metacriticScore>span')?.textContent,
        runtime: document.querySelector('.title_wrapper>.subtext>time')?.textContent.trim(),
        genres: [...document.querySelectorAll('#titleStoryLine .see-more>a[href*="genres"]')].map((e) => e.textContent.trim()),
        keywords: [...document.querySelectorAll('#titleStoryLine .see-more>a[href*="keywords"]')].map((e) => e.textContent.trim()),
        language: [...document.querySelectorAll('#titleDetails .txt-block> a[href*="language"]')].map((e) => e.textContent),
        producers: [...document.querySelectorAll('#titleDetails .txt-block> a[href*="company/co"]')].map((e) => ({ imdbId: e.href.match(/co[0-9]+/)[0], name: e.textContent.trim() })),
        country: [...document.querySelectorAll('#titleDetails>.txt-block a[href*="country_of_origin"]')].map((e) => e.textContent),
        releaseDates: document.querySelector('.title_wrapper>.subtext>a[title*="release dates"]')?.textContent.replace(/[ ]{2,}/g, ' ').trim(),
        cast: [...document.querySelectorAll('#titleCast .cast_list td:nth-of-type(2)>a[href*="name"]')].map((e) => ({ imdbId: e.href.match(/nm[0-9]+/)[0], name: e.textContent.trim() })),
        locations: document.querySelector('#titleDetails>.txt-block>a[href*="locations"]')?.textContent.replace(/,\s/g, ',').split(','),
        //summary: document.querySelector('.plot_summary .summary_text')?.textContent.trim(),
        summary: document.querySelector('#titleStoryLine>.canwrap>p')?.textContent.trim(),
        credits: [...document.querySelectorAll('.plot_summary .credit_summary_item')].reduce((credit, elm) => ({
            ...credit, [`${elm.querySelector('.inline').textContent.toLowerCase().replace(':', '')}`]:
                [...elm.querySelectorAll('a[href^="/name"]')].map((e) => ({ imdbId: e.href.match(/nm[0-9]+/)[0], name: e.textContent.trim() }))
        }), {}),
    }
};

function elapsed() {
    return Math.round(((Date.now() - startTime) / 1000) / 60);
}