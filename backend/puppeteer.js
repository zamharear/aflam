const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
    userDataDir: './cache',
    args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage', '--disable-gpu']
  });
  const page = await browser.newPage();
  await page.setJavaScriptEnabled(false);
  await page.goto('https://www.imdb.com/title/tt2306299/');
  //const showCast = await page.$('.cast_list');
  const showCast = await page.$$eval('table.cast_list .odd>td:nth-of-type(2)>a,table.cast_list .even>td:nth-of-type(2)>a', all => all.map(elm => ({ id: elm.href.match(/nm[0-9]+/)[0], name: elm.innerText })));
  console.log(showCast);

  //await page.screenshot({path: 'example.png'});

  await browser.close();
})();