export type TShow = {
    id: number,
    url: string,
    name: string,
    type: string,
    language: string,
    genres?: string[],
    status: string,
    runtime: number,
    premiered: string,
    officialSite?: string,
    schedule: TSchedule,
    rating: TRating,
    network?: TNetwork,
    weight?: number,
    webChannel?: any,
    externals?: TExternal,
    image: TImage,
    summary: string,
    updated: number,
    crew?: TCrew,
    cast?: TCast,
    timeRemainedUntilNext?: number,
    _links: {
        self: {
            href: string
        },
        previousepisode?: {
            href: string
        },
        nextepisode?: {
            href: string
        }
    }
};

export type TSeason = {
    id: number,
    url: string,
    number: number,
    name: string,
    episodeOrder: number,
    premiereDate: string,
    endDate: string,
    network: TNetwork,

    image: TImage,
    summary: string,
    _links: {
        self: {
            href: string
        }
    }
};

export type TEpisode = {

    id: number,
    url: string,
    name: string,
    season: number,
    number: number,
    airdate: string,
    airtime: string,
    airstamp: string,
    runtime: number,
    image: TImage
    summary: string,
    _links: {
        self: {
            href: string
        }
    }

};

type TSchedule = {
    time: string,
    days: string[]
};

type TRating = {
    average: number

};

type TNetwork = {
    id: number,
    name: string,
    country: TCountry
};

type TCountry = {
    name: string,
    code: string,
    timezone: string
};

type TExternal = {
    tvrage?: number,
    thetvdb?: number,
    imdb?: string
};

type TImage = {
    medium?: string,
    original?: string
};
export type TCrewItem = {
    type: string,
    person: TPerson
};
type TCrew = TCrewItem[];

export type TCastItem = {
    person: TPerson,
    character: TCharacter,
    self: boolean,
    voice: boolean
};
type TCast = TCastItem[];

export type TCharacter = {
    id: number,
    url: string,
    name: string,
    image: TImage,
    _links: {
        self: {
            href: string
        }
    }

};
export type TPerson = {
    id: number,
    url: string,
    name: string,
    country?: TCountry,
    birthday?: string,
    deathday?: string,
    gender: string,
    image?: TImage,
    _links: {
        self: {
            href: string
        }
    }
};
