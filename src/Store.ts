import { observable, runInAction, action } from "mobx";
import { TShow, TSeason, TEpisode } from "./types";
import React from "react";
import yifyList from "./dummy_data/yify_movies.json";

const apiurl = "https://api.tvmaze.com";
const imdbsuggestion = "http://localhost:8000";
// search/shows?q=girls

type TResult = {
  score: number;
  show: TShow;
};

type TState = {
  query: string;
  results: TResult[];
  selected: TResult | undefined;
  pEpisode: TEpisode | undefined;
  nEpisode: TEpisode | undefined;
};

class Store {
  query = "";
  @observable
  yifyList = yifyList;
  @observable
  results: TResult[] = [];
  @observable
  selected: TResult | undefined;
  @observable
  pEpisode: TEpisode | undefined;
  @observable
  nEpisode: TEpisode | undefined;
  @observable
  seasons: TSeason[] = [];
  @observable
  selectedSeason: number | undefined;
  @observable
  episodes: TEpisode[] = [];
  @observable
  favorites: number[] = [];
  @action
  clearQuery = () => {
    console.log("ok working");
    this.query = "";
  };
  constructor() {
    //load favoriets from store
    const strFav = localStorage.getItem("favoriets");
    if (strFav) {
      this.favorites = JSON.parse(strFav);
    }
  }
  getMovie = (name: string) => {
    //@ts-ignore
    return this.yifyList[name];
  };
  fillMovie = (name: string) => {
    fetch(`${imdbsuggestion}/${name.charAt(0)}/${name}.json`)
      .then((r) => r.json())
      .then((data) => {
        console.log(data);
        //@ts-ignore
        runInAction(() => this.yifyList[name] = data);
      });
  };

  search = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key !== "Enter") return;
    const target = (e.nativeEvent.target as HTMLInputElement);
    // target.value = '';
    target.blur();
    this.closeShow();
    fetch(apiurl + `/search/shows?q=${encodeURIComponent(this.query)}`)
      .then((r) => r.json())
      .then((data) => {
        runInAction(() => this.results = data);
      });
  };

  changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.query = e.target.value;
  };
  timeRemainedUntilNextEpesode = (showId: number) => {
    // const resDx = this.results.findIndex(v => v.show.id === showId);
    const show = this.results.find((v) => v.show.id === showId)?.show;
    if (!show?._links.nextepisode) return;
    const episodeId = show._links.nextepisode.href.replace(/[^0-9]+/, "");
    fetch(apiurl + `/episodes/${episodeId}`)
      .then((r) => r.json())
      .then((data) =>
        runInAction(() => {
          show.timeRemainedUntilNext = Math.round(
            (
              (new Date(data.airdate)).getTime() - Date.now()
            ) / (1000 * 60 * 60 * 24),
          );
        })
      );
  };
  openShow = (id: number) => {
    const selected = this.results.find((r) => r.show.id === id);
    if (selected?.show._links.previousepisode) {
      this.fetchEpisode(
        parseInt(
          selected.show._links.previousepisode.href.replace(/[^0-9]+/, ""),
        ),
      );
    }
    if (selected?.show._links.nextepisode) {
      this.fetchEpisode(
        parseInt(selected.show._links.nextepisode.href.replace(/[^0-9]+/, "")),
        true,
      );
    }
    this.selected = selected;
    this.fetchSeasons(id);
  };

  closeShow = () => {
    this.selected = undefined;
    this.seasons = [];
    this.episodes = [];
    this.nEpisode = undefined;
    this.pEpisode = undefined;
    this.selectedSeason = undefined;
  };

  fetchSeasons = (showId: number) => {
    fetch(apiurl + `/shows/${showId}/seasons`)
      .then((r) => r.json())
      .then((data) => runInAction(() => this.seasons = data));
  };

  fetchSeasonEpisodes = (seasonId: number) => {
    fetch(apiurl + `/seasons/${seasonId}/episodes`)
      .then((r) => r.json())
      .then((data) =>
        runInAction(() => {
          this.episodes = data;
          this.selectedSeason = seasonId;
        })
      );
  };

  fetchEpisode = (showId: number, next = false) => {
    fetch(apiurl + `/episodes/${showId}`)
      .then((r) => r.json())
      .then((data) =>
        runInAction(() => this[next ? "nEpisode" : "pEpisode"] = data)
      );
  };

  fetchCrew = (showId: number) => {
    fetch(apiurl + `/shows/${showId}/crew`)
      .then((r) => r.json())
      .then((data) =>
        runInAction(() => {
          if (this.selected) {
            this.selected.show.crew = data;
          }
        })
      );
  };

  fetchCast = (showId: number) => {
    fetch(apiurl + `/shows/${showId}/cast`)
      .then((r) => r.json())
      .then((data) =>
        runInAction(() => {
          if (this.selected) {
            this.selected.show.cast = data;
          }
        })
      );
  };

  @action
  favoritesAddRemove = (showId: number) => {
    if (this.favorites.includes(showId)) {
      this.favorites.splice(this.favorites.indexOf(showId), 1);
    } else if (this.selected?.show.status.toLowerCase() !== "ended") {
      //only if not ended
      this.favorites.push(showId);
    }
    localStorage.setItem("favoriets", JSON.stringify(this.favorites));
  };

  loadFavorits = () => {
    this.results = [];
    this.closeShow();
    this.favorites.forEach((showId) => {
      fetch(apiurl + `/shows/${showId}`)
        .then((r) => r.json())
        .then((data) =>
          runInAction(() =>
            this.results = [...this.results, { score: -1, show: data }]
          )
        );
    });
  };

  copyToClipboard = (text: string) => {
    const textArea = document.createElement("textarea");

    textArea.setAttribute(
      "style",
      `position:fixed;
            top:0;
            left:0;
            width:2rem;
            height:2rem;
            padding:0;
            border:none;
            outline:none;
            box-shadow:none;
            background:transparent;`,
    );
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
      const successful = document.execCommand("copy");
    } catch (err) {
      console.log("Oops, unable to copy");
    }

    document.body.removeChild(textArea);
  };
}

const storeContext = React.createContext(new Store());
export const useStore = () => React.useContext(storeContext);
