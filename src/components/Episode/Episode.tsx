import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import { TEpisode } from '../../types';
import styles from './Episode.module.scss';
import { useStore } from '../../Store';

const noImage = 'https://static.tvmaze.com/images/no-img/no-img-landscape-text.png';
export const Episode: React.FC<{ props: TEpisode }> = ({ props }) => {
    const store = useStore();
    const episodeNo = ('00' + props.number).slice(-2);
    const seasonNo = ('00' + props.season).slice(-2);
    return (
        <div className={styles.episode}>
            <div className={styles.name}>
                <span onClick={() => store.copyToClipboard(`${store.selected?.show.name.replace(/\W/g, '.')}.s${seasonNo}.e${episodeNo}`)}>#{episodeNo} | </span>
                <b>{props.name}: </b>
                <span>{props.airdate.split('-').reverse().join(':')}</span>
            </div>
            <img src={props.image?.medium || noImage} alt={props.name} />
            {ReactHtmlParser(props.summary)}

        </div>
    )
}


export const MiniEpisode: React.FC<TEpisode> = ({ name, airdate }) => {
    return (
        <div><b>{name}</b> [{airdate}]</div>
    )
}