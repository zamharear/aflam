import React from 'react';
import { TSeason } from '../../types';
import styles from './Season.module.scss';
import { useStore } from '../../Store';
import { useObserver } from 'mobx-react';

export const Season: React.FC<{ props: TSeason }> = ({ props }) => {
    const store = useStore();
    //console.log(store.episodes[0]?.season,props.id);
    return useObserver(() => (
        <div
            className={styles.content + ' ' + (store.selectedSeason === props.id ? styles.active : '')}
            onClick={() => store.fetchSeasonEpisodes(props.id)}
        >
            <strong>{props.number} </strong>
            {props.episodeOrder ? <span>[{props.episodeOrder}]</span> : false}
        </div>
    ));
}