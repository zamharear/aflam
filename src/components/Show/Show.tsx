import React from 'react';
import { TShow, TSeason, TEpisode } from '../../types';
import styles from './Show.module.scss';
import ReactHtmlParser from 'react-html-parser';
import { Close } from './Close';
import { Season } from '../Season';
import { Episode } from '../Episode';
import { useStore } from '../../Store';
import { useObserver } from 'mobx-react';
import { Actor } from '../Actor';

type Pops = {
    show: TShow,
    seasons: TSeason[],
    pEpisode: TEpisode | undefined,
    nEpisode: TEpisode | undefined,
    episodes: TEpisode[],
    closeShow: () => void
}

export const Show: React.FC<Pops> = ({ show, seasons, pEpisode, nEpisode, episodes, closeShow }) => {
    const l = show.genres?.length || 0;
    const store = useStore();
    return useObserver(() => (
        <section className={`${styles.content} ${(store.favorites.includes(show.id) ? styles['content--favorite'] : '')}`}>
            <section className={styles.info}>
                <figure>
                    <img src={show.image?.medium} alt={show.name} />
                    <span onClick={() => store.favoritesAddRemove(show.id)}></span>
                </figure>
                <aside>
                    <div>
                        <strong>Network: </strong>
                        <span>{show.network?.name} </span>
                        <span>({show.premiered})</span>
                    </div>

                    <div>
                        <strong>Schedule: </strong>
                        <span>{show.schedule.days} </span>
                        <span>at {show.schedule.time} </span>
                        <span>({show.runtime} min)</span>
                    </div>
                    <div><strong>Status: </strong>{show.status}</div>
                    <div><strong>Show Type: </strong> {show.type}</div>
                    <div>
                        <strong>Genres: </strong>
                        <span className="genres">
                            {
                                show.genres?.map((g, i) =>
                                    (<span key={i}> {g + (i < l - 1 ? ',' : '')} </span>))
                            }
                        </span>
                    </div>
                    {
                        pEpisode ?
                            <div>
                                <strong>Previous: #{pEpisode.number} </strong>
                                <span>{pEpisode.name}</span>,
                            <span className={styles.date}> {pEpisode.airdate.split('-').reverse().join(':')}</span>
                            </div> :
                            false
                    }
                    {
                        nEpisode ?
                            <div>
                                <strong>Next: #{nEpisode.number} </strong>
                                <span>{nEpisode.name}</span>,
                            <span className={styles.date} > {nEpisode.airdate.split('-').reverse().join(':')}</span>
                            </div> :
                            false
                    }
                    {
                        show.officialSite ?
                            <div>
                                <strong>Official site: </strong>
                                <a
                                    href={show.officialSite}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {show.officialSite}
                                </a>
                            </div> :
                            false
                    }
                    {
                        show.externals?.imdb ?
                            <div>
                                <strong onClick={() => store.copyToClipboard(`${show.externals?.imdb || ''}`)}>IMDB: </strong>
                                <a
                                    href={`https://www.imdb.com/title/${show.externals.imdb}/`}
                                    target="_blank"
                                >
                                    {show.externals.imdb}
                                </a>
                            </div> :
                            false
                    }
                    <article>{ReactHtmlParser(show.summary)}</article>
                    <button onClick={() => store.fetchCast(show.id)}>cast</button>
                </aside>
            </section>
            <section className={styles.cast}>
                {show.cast?.map(actor => <Actor key={actor.person.id} props={actor} />)}
            </section>
            <section className={styles.seasons}>
                {seasons.map(season => <Season key={season.id} props={season} />)}
            </section>
            <section className={styles.episodes}>
                {episodes.slice().reverse().map(e => (
                    <Episode key={e.id} props={e} />
                ))}
            </section>

            <Close onClick={closeShow} />
        </section >
    ));
};