import React, { useState } from 'react';
import styles from './Close.module.scss';

type Props = {
    onClick: () => void
}

export const Close: React.FC<Props> = ( { onClick } ) => {
    const [ active, setActive ] = useState( false );
    return (
        <div
            className={ `${styles.btn} ${( active ? styles[ 'btn--active' ] : '' )}` }
            onMouseEnter={ () => setActive( true ) }
            onMouseLeave={ () => setActive( false ) }
            onClick={ onClick }
        >
            <span className={ styles.span }></span>
            <span className={ styles.span }></span>
            <span className={ styles.span }></span>
            <span className={ styles.span }></span>
            <div className={ styles.lable }>close</div>
        </div>
    );
}