import React from 'react'
import styles from './Search.module.scss';
type props = {
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    search: (e: React.KeyboardEvent<HTMLInputElement>) => void
}
export const Search: React.FC<props> = ({ onChange, search }) => {
    const ref = React.createRef<HTMLInputElement>();
    return (
        <section className={styles.searchboxWrap}>
            <input
                ref={ref}
                onChange={onChange}
                onKeyPress={search}
                type="text"
                className={styles.searchbox}
            />
            <span onClick={() => { if (ref.current) { ref.current.value = '' } }} />
        </section>
    )
};