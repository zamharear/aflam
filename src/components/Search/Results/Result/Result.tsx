import React from 'react';
import { TShow } from '../../../../types';
import styles from './Result.module.scss';
import { useStore } from '../../../../Store';
import { useObserver } from 'mobx-react';

type Props = {
    result: { score: number, show: TShow },
    openShow: (id: number) => void
}
export const Result: React.FC<Props> = ({ result, openShow }) => {
    const store = useStore();
    const { show } = result;
    return useObserver(() => (
        <div className={styles.result}>
            <img src={show.image?.medium} alt={show.name} onClick={() => openShow(show.id)} />
            <h3 onClick={() => store.copyToClipboard(show.name)}>{show.name}</h3>
            <div>
                <div> <span>({show.premiered?.split('-').reverse().join('.')}) | </span><strong>{show.status}</strong></div>
                { show.status.toLowerCase() === 'ended' ? '' :
                    <div onClick={() => store.timeRemainedUntilNextEpesode(show.id)}>
                        Next Ep: {show.timeRemainedUntilNext ? show.timeRemainedUntilNext + ' days' : 'unknown!'}
                    </div>
                }
            </div>
        </div>
    ))
}