import React from 'react';
import { TShow } from '../../../types';
import { Result } from './Result/';
import styles from './Results.module.scss';
type Result = {
    score: number,
    show: TShow
}
type Props = {
    results: Result[],
    openShow: ( id: number ) => void
}

export const Results: React.FC<Props> = ( { results, openShow } ) => {
    return (
        <section className={ styles.results }>
            { results.map( result => <Result key={ result.show.id } result={ result } openShow={ openShow } /> ) }
        </section>
    )
}