import React from 'react';
import styles from './Calendar.module.scss';
export const Calendar: React.FC = () => {
    const date = new Date();

    //return `${year}${separator}${month<10?`0${month}`:`${month}`}${separator}${date}`
    return (
        <span className={ styles.calendar }>
            { `${( '00' + date.getDate() ).slice( -2 )}:${( '00' + ( date.getMonth() + 1 ) ).slice( -2 )}:${date.getFullYear()}` }
        </span>
    );
};