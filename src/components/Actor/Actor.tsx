import React from 'react';
import { TCastItem } from '../../types';
import styles from './Actor.module.scss';

export const Actor: React.FC<{ props: TCastItem }> = ({ props }) => {
    //const store = useStore();
    return (
        <div className={styles.actor}>
            <figure><img src={props.person.image?.medium} /></figure>
            <div>
                <div>{props.person.name}:{props.person.birthday ?
                    Math.floor((Date.now() - (new Date(props.person.birthday)).getTime()) / (1000 * 60 * 60 * 24 * 365.25)) : null
                }</div>
                <div className={styles.cname}>{props.character.name}</div>
                <div>{props.person.country?.name}</div>
            </div>
            {/* <div>{props.character.name}</div>
            <figure><img src={props.character.image.medium} /></figure> */}
        </div>
    );
};