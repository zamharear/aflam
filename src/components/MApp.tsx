import React from 'react';
import { Search } from './Search/';
import { Results } from './Search/Results/Results';
import { Show } from './Show';
import { useStore } from '../Store';
import { observer } from 'mobx-react'
import { Calendar } from './Calendar';
import { MovieItem } from './MovieItem';

function App() {
    const store = useStore();
    return (
        <div className="App">
            <header>
                <h1> Movie Spotter <Calendar /> </h1>
                <section style={ { color: 'white' } }>
                    { Object.keys(store.yifyList).map((title, i) => store.getMovie(title).imdbId ?
                        <MovieItem key={ i } props={ store.getMovie(title) } /> :
                        <div key={ i } style={ { cursor: 'pointer' } } onClick={ () => store.fillMovie(title) }>{ title }</div>)
                    }
                </section>
            </header>
            <main>

            </main>

        </div>
    );
}

export default observer(App);
