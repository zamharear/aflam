import React from 'react';
import { Search } from './Search/';
import { Results } from './Search/Results/Results';
import { Show } from './Show';
import { useStore } from '../Store';
import { observer } from 'mobx-react'
import { Calendar } from './Calendar';
function App() {
  const store = useStore();
  return (
    <div className="App">
      <header>
        <h1> Movie Spotter <Calendar /> </h1>
      </header>
      <main>
        <Search onChange={ store.changeHandler } search={ store.search } />
        <h3 style={ { display: 'none' } }>8F*ymwT$[:_#brDR</h3>
        <button onClick={ store.loadFavorits }>Favorites</button>
        {
          (store.selected) ?
            <Show
              show={ store.selected.show }
              seasons={ store.seasons }
              nEpisode={ store.nEpisode }
              pEpisode={ store.pEpisode }
              episodes={ store.episodes }
              closeShow={ store.closeShow }
            /> :
            <Results results={ store.results } openShow={ store.openShow } />
        }
      </main>

    </div>
  );
}

export default observer(App);
