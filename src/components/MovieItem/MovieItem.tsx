import React from 'react';
import styles from './MovieItem.module.scss';
//import { useStore } from '../../Store';
/*
{
height: 2500,
imageUrl: "https://m.media-amazon.com/images/M/MV5BNDliY2E1MjUtNzZkOS00MzJlLTgyOGEtZDg4MTI1NzZkMTBhXkEyXkFqcGdeQXVyNjMwMzc3MjE@._V1_.jpg",
width: 1688
},
id: "tt4566758",
l: "Mulan",
q: "feature",
rank: 4,
s: "Yifei Liu, Donnie Yen",
v: [],
vt: 34,
y: 2020
*/
type TMovieItem = any;
export const MovieItem: React.FC<{ props: TMovieItem }> = ({ props }) => {
    return (
        <section className={ styles.content }>
            <section className={ styles.info }>
                <figure><img src={ props.poster } alt={ props.name } /></figure>
                <aside>
                    <h3>{ props.name } ({ props.year })</h3>
                    <div>
                        <strong><a href={ `https://www.imdb.com/title/${props.imdbId}/` } target="_blank">Imdb:</a> </strong>
                        <span>{ props.rating } / { props.raters }</span>
                    </div>
                    {
                        props.metascore ?
                            <div>
                                <strong>Metascore: </strong>
                                <span>{ props.metascore }</span>
                            </div> :
                            null
                    }
                    <div>
                        <strong>Genres: </strong>
                        <span className="genres">
                            {
                                props.genres?.map((v: string, i: number) =>
                                    <span key={ i }> { v } </span>)
                            }
                        </span>
                    </div>
                    <div>
                        <strong>Keywords: </strong>
                        <span className="genres">
                            {
                                props.keywords?.map((v: string, i: number) =>
                                    <span key={ i }> { v } </span>)
                            }
                        </span>
                    </div>
                    <div>
                        <strong>Release: </strong>
                        <span>{ props.releaseDates }</span>
                    </div>

                    {
                        Object.keys(props.credits).map((title, i) => (
                            <div key={ i }>
                                <strong>{ title }:</strong>
                                {
                                    props.credits[ title ].map((v: any, j: number) => <span key={ j }> { v.name }</span>)
                                }
                            </div>
                        ))
                    }

                    <article>{ props.summary }</article>
                </aside>
            </section>
        </section>
    )
}
